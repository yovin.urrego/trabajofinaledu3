/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package vista.sumKidsVista;

import controlador.ctrlSunKidsClub.controladorGerente;
import controlador.ctrlSunKidsClub.controladorSecretaria;
import controlador.tda.lista.ListaEnlazada;
import controlador.tda.lista.exception.PosicionException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.SumKids.Estudiantes;
import modelo.SumKids.Profesores;

/**
 *
 * @author Jordy
 */
public class FrmGerente extends javax.swing.JFrame {

    private controladorGerente gerente = new controladorGerente();
    private controladorSecretaria secretaria = new controladorSecretaria();
    private ListaEnlazada<Profesores> lP = new ListaEnlazada();
    private ListaEnlazada<Estudiantes> lE = new ListaEnlazada();
    private Integer[][] lA;

    /**
     * Creates new form GerenteFrame
     */
    public FrmGerente() throws PosicionException, SQLException {
        try {
            this.lA = new Integer[3][gerente.contar()];
        } catch (SQLException ex) {
            Logger.getLogger(FrmGerente.class.getName()).log(Level.SEVERE, null, ex);
        }

        initComponents();
        this.setTitle("Aprobar Estudiantes");
        rsscalelabel.RSScaleLabel.setScaleLabel(jLabel1, "src/vista/imagenes/Fondo3.jpeg");
        this.setLocationRelativeTo(null);
        // this.Paneltable2.setVisible(false);
        mostaraTablaProfesor();
        mostrarTablaEstudiante();
        tblRelacion.setModel(cargarTablaRelaciones());
    }

    public void mostrarTablaEstudiante() {
        try {
            tblEstudiante.setModel(this.cargarTablaEstudiantes());
        } catch (PosicionException ex) {
            Logger.getLogger(FrmEstudiante.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para agisnar los datos en la tabla de estudiantes
     *
     * @return
     * @throws PosicionException
     */
    public DefaultTableModel cargarTablaEstudiantes() throws PosicionException {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Id");
        model.addColumn("Identificacion");
        model.addColumn("Nombre");
        model.addColumn("Apellido");
        model.addColumn("Genero");
        model.addColumn("Direccion");
        model.addColumn("Pension");
        model.addColumn("Fecha Nac.");

        lE = secretaria.listarEstudiantes();
        String datos[] = new String[8];
        for (int i = 0; i < lE.getSize(); i++) {
            datos[0] = String.valueOf(lE.obtenerDato(i).getId_estudiante());
            datos[1] = lE.obtenerDato(i).getIdentificacion();
            datos[2] = lE.obtenerDato(i).getNombres();
            datos[3] = lE.obtenerDato(i).getApellidos();
            datos[4] = String.valueOf(lE.obtenerDato(i).getGenero());
            datos[5] = lE.obtenerDato(i).getDireccion();
            datos[6] = String.valueOf(lE.obtenerDato(i).getPension());
            datos[7] = String.valueOf(lE.obtenerDato(i).getFechaNacimiento());
            model.addRow(datos);
        }
        return model;
    }

    public DefaultTableModel cargarTablaRelaciones() throws PosicionException, SQLException {
        DefaultTableModel model = new DefaultTableModel();
        try{
        model.addColumn("Id");
        model.addColumn("Id_Estudiante");
        model.addColumn("Id_profesor");
        lA = this.gerente.asignaciones();
        String datos[] = new String[3];
           
        for (int i = 0; i < gerente.contar(); i++) {
          
            System.out.println(gerente.contar());
            datos[0] = String.valueOf(lA[0][i]); 
            datos[1] = String.valueOf(lA[1][i]);
            datos[2] = String.valueOf(lA[2][i]);
            model.addRow(datos);
        }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "No se pudo realizar la accion", "Advertencia", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    /**
     * Metodo para llamar la tabla de profesores
     */
    public void mostaraTablaProfesor() {
        try {
            tablaAprobacion.setModel(this.cargarTablaProfesores());
        } catch (PosicionException ex) {
        JOptionPane.showMessageDialog(null, "No se pudo realizar la accion", "Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metodo para cargar los datos en la tabla de profesores
     *
     * @return
     * @throws PosicionException
     */
    public DefaultTableModel cargarTablaProfesores() throws PosicionException {
      DefaultTableModel model = new DefaultTableModel();
       try {
             
       
        model.addColumn("Id");
        model.addColumn("Nombre");
        model.addColumn("Apellido");
        model.addColumn("Telefono");
        model.addColumn("Correo");
        model.addColumn("Direccion");
        model.addColumn("Especialidad");
        model.addColumn("Genero");
        lP = secretaria.listarProfesores();
        String datos[] = new String[8];
        for (int i = 0; i < lP.getSize(); i++) {
            datos[0] = String.valueOf(lP.obtenerDato(i).getId_profesor());
            datos[1] = lP.obtenerDato(i).getNombres();
            datos[2] = lP.obtenerDato(i).getApellidos();
            datos[3]= String.valueOf(lP.obtenerDato(i).getCelular());
            datos[4] = lP.obtenerDato(i).getCorreo();
            datos[5] = lP.obtenerDato(i).getDireccion();
            datos[6] = String.valueOf(lP.obtenerDato(i).getEspecialidad());
            datos[7] = String.valueOf(lP.obtenerDato(i).getGenero());
            model.addRow(datos);
        }
       }catch(Exception e){
        JOptionPane.showMessageDialog(null, "No se pudo realizar la accion", "Advertencia", JOptionPane.ERROR_MESSAGE);    
       }
           
        return model;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        botaprobar = new javax.swing.JButton();
        btnAtras = new javax.swing.JButton();
        Paneltable2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaAprobacion = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblEstudiante = new javax.swing.JTable();
        cmdCerrar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblRelacion = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setDoubleBuffered(false);
        jPanel1.setLayout(null);

        botaprobar.setBackground(new java.awt.Color(255, 51, 51));
        botaprobar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        botaprobar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/edit.png"))); // NOI18N
        botaprobar.setText("Asignar");
        botaprobar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaprobarActionPerformed(evt);
            }
        });
        jPanel1.add(botaprobar);
        botaprobar.setBounds(40, 180, 110, 30);

        btnAtras.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/door.png"))); // NOI18N
        btnAtras.setText("Atrás");
        btnAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasActionPerformed(evt);
            }
        });
        jPanel1.add(btnAtras);
        btnAtras.setBounds(170, 180, 120, 30);

        Paneltable2.setBackground(new java.awt.Color(204, 255, 255));
        Paneltable2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3), "Lista profesores", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        Paneltable2.setOpaque(false);
        Paneltable2.setLayout(null);

        tablaAprobacion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaAprobacion.addHierarchyBoundsListener(new java.awt.event.HierarchyBoundsListener() {
            public void ancestorMoved(java.awt.event.HierarchyEvent evt) {
                tablaAprobacionAncestorMoved(evt);
            }
            public void ancestorResized(java.awt.event.HierarchyEvent evt) {
            }
        });
        jScrollPane2.setViewportView(tablaAprobacion);

        Paneltable2.add(jScrollPane2);
        jScrollPane2.setBounds(20, 20, 450, 100);

        jPanel1.add(Paneltable2);
        Paneltable2.setBounds(20, 220, 490, 140);

        jPanel2.setBackground(new java.awt.Color(204, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3), "Lista Estudiantes", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel2.setOpaque(false);
        jPanel2.setLayout(null);

        tblEstudiante.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblEstudiante);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(30, 30, 420, 100);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(10, 20, 490, 150);

        cmdCerrar.setBackground(new java.awt.Color(255, 255, 255));
        cmdCerrar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cmdCerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/del.png"))); // NOI18N
        cmdCerrar.setToolTipText("Eliminar");
        cmdCerrar.setPreferredSize(new java.awt.Dimension(120, 25));
        cmdCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdCerrarActionPerformed(evt);
            }
        });
        jPanel1.add(cmdCerrar);
        cmdCerrar.setBounds(860, 0, 60, 30);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3), "Profesores asignados"));
        jPanel3.setOpaque(false);

        tblRelacion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tblRelacion);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel3);
        jPanel3.setBounds(520, 50, 390, 280);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/imagenes/Fondo3.jpeg"))); // NOI18N
        jLabel1.setText("jLabel1");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(0, 0, 950, 370);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 948, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botaprobarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaprobarActionPerformed
        int filaPro = tablaAprobacion.getSelectedRow();
        int filaEst = tblEstudiante.getSelectedRow();

        try {
            lP.obtenerDato(filaPro).getId_profesor();
            lE.obtenerDato(filaEst).getId_estudiante();
            gerente.AsignarDocente(filaPro, filaEst);
            this.tblRelacion.setModel(this.cargarTablaRelaciones());
        } catch (PosicionException ex) {
             JOptionPane.showMessageDialog(null, "No se pudo realizar la accion", "Advertencia", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(FrmGerente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null, "No se pudo realizar la accion", "Advertencia", JOptionPane.ERROR_MESSAGE);
       
            Logger.getLogger(FrmGerente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_botaprobarActionPerformed

    private void tablaAprobacionAncestorMoved(java.awt.event.HierarchyEvent evt) {//GEN-FIRST:event_tablaAprobacionAncestorMoved

    }//GEN-LAST:event_tablaAprobacionAncestorMoved

    private void btnAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasActionPerformed
        new FrmIngreso().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnAtrasActionPerformed

    private void cmdCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdCerrarActionPerformed
        System.exit(0);
    }//GEN-LAST:event_cmdCerrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new FrmGerente().setVisible(true);
                } catch (PosicionException ex) {
                    Logger.getLogger(FrmGerente.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(FrmGerente.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Paneltable2;
    private javax.swing.JButton botaprobar;
    private javax.swing.JButton btnAtras;
    private javax.swing.JButton cmdCerrar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tablaAprobacion;
    private javax.swing.JTable tblEstudiante;
    private javax.swing.JTable tblRelacion;
    // End of variables declaration//GEN-END:variables
}
