/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.dao;

import controlador.conexion.SQLclass;
import controlador.tda.lista.ListaEnlazada;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.SumKids.Representantes;

/**
 *
 * @author Jordy
 */
public class RepresentanteDao extends AdaptadorDao<Representantes> {

    private Representantes representante;

    public Representantes getRepresentante() {
        if (representante == null) {
            representante = new Representantes();

        }
        return representante;
    }

    public void setRepresentante(Representantes em) {
        this.representante = em;
    }

    public RepresentanteDao() {
        super(Representantes.class);
    }

    public Boolean guardarModificar() {
        try {
            if (this.getRepresentante().getId_representante() != null) {
                modificar(this.getRepresentante());

            } else {
                guardar(this.getRepresentante());
            }
            return true;
        } catch (Exception e) {
            System.out.println("Error" + e);
            return false;
        }
    }

    public void guardarRepresentante(Object dato) throws SQLException {
        Representantes Repre = (Representantes) dato;
        String[] columnas = super.columnas();
        String comando = "insert into Representantes";
        String variables = "";
        System.out.println("*******************");
        for (int i = 0; i < columnas.length; i++) {
            if (i == columnas.length - 1) {
                variables += columnas[i];

            } else {
                variables += columnas[i] + " , ";
            }
        }

        comando += "(" + variables + ") values("+ (super.contar() + 1) + ",'" + Repre.getNombres()+ "','" + Repre.getApellidos()+ "','" + Repre.getIdentificacion() + "','" + Repre.getCelular() + "','" + Repre.getGenero() + "','" + Repre.getDireccion() + "')";
        try {
            PreparedStatement stmt = getConexion().prepareStatement(comando);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "guardado correctamente");
        } catch (SQLException ex) {
            System.out.println("Error en guardar " + ex);
        }
        System.out.println(comando);
        commit();
    }
    
    public ListaEnlazada<Representantes> getRepresentantes(String atributo, Object o, Boolean m) {
        ListaEnlazada<Representantes> listaRepresentantes = new ListaEnlazada<>();

        String sql;
        if (m) {
           sql = "SELECT * FROM representantes " + atributo + " = " +o+ " order by 1";
            System.out.println(sql);
        } else {
            sql = "SELECT * FROM representantes where " + atributo + " ='"+ o +"' order by 1";
            System.out.println(sql);
        }

    try {
            PreparedStatement pps = getConexion().prepareStatement(sql);
            ResultSet resultado = pps.executeQuery();
            while (resultado.next()) {
                Representantes representante = new Representantes(resultado.getInt("Id_representante"), resultado.getString("nombre"), resultado.getString("apellido"),resultado.getString("identificacion"),resultado.getString("telefono") ,resultado.getString("genero").charAt(0), resultado.getString("direccion"));
                listaRepresentantes.insertarCabecera(representante);
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        return listaRepresentantes;
    }
    
    /**
     * 
     */
    public void updateRepresentante(String nombre, String apellido, String telefono, char genero, String direccion, String cedula) throws Exception {
        
        System.out.println(nombre + apellido + telefono + genero + direccion + cedula);
        String update = "Update representantes set nombre ='" + nombre + "',apellido ='" + apellido + "',telefono ='" + telefono + "', genero='" + genero + "',direccion='" + direccion + "' where identificacion = '" + cedula + "'";

        System.out.println(update);
        try {
            PreparedStatement stmt = getConexion().prepareStatement(update);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "actualizado correctamente");
        } catch (SQLException ex) {
            System.out.println("Error en guardar " + ex);
        }
        commit();
    }
    public void listarRepresentante(Connection con, JTable tabla) {
        DefaultTableModel model;
        String[] columnas = {"ID_REPRESENTANTE", "NOMBRE", "APELLIDO", "IDENTIFICACION", "TELEFONO", "GENERO", "DIRECCION"};
        model = new DefaultTableModel(null, columnas);

        String comando = "SELECT * FROM REPRESENTANTES ORDER BY ID_REPRESENTANTE";

        String[] filas = new String[7];
        Statement st = null;
        ResultSet rs = null;

        try {
            st = con.createStatement();
            rs = st.executeQuery(comando);
            while (rs.next()) {
                for (int i = 0; i < 7; i++) {
                    filas[i] = rs.getString(i + 1);
                }
                model.addRow(filas);
            }
            tabla.setModel(model);

            getConexion().close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No se puede listar los datos en la tabla");
        }
    }

    public void listRepre(JTable tabla) {
        Connection con = SQLclass.getConection();
        listarRepresentante(con, tabla);
        try {
            con.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
