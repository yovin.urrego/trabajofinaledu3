/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.dao;

import controlador.conexion.SQLclass;
import controlador.tda.lista.ListaEnlazada;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import javax.swing.table.DefaultTableModel;
import modelo.SumKids.Profesores;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modelo.SumKids.Empleados;
import modelo.enums.TipoEmpleado;

/**
 *
 * @author Jordy
 * @param <T>
 */
public class ProfesorDao extends AdaptadorDao<Profesores> {

    private Profesores profesor;
    SQLclass s = new SQLclass();
    Connection con = s.getConection();

    public Profesores getProfesor() {
        if (profesor == null) {
            profesor = new Profesores();

        }
        return profesor;
    }

    public void setProfesor(Profesores em) {
        this.profesor = em;
    }

    public ProfesorDao() {
        super(Profesores.class);
    }

    public Boolean guardarModificar() {
        try {
            if (this.getProfesor().getId_profesor() != null) {
                modificar(this.getProfesor());

            } else {
                guardar(this.getProfesor());
            }
            return true;
        } catch (Exception e) {
            System.out.println("Error" + e);
            return false;
        }
    }

    public void guardarProfesor(Object dato) throws Exception {
        Profesores p = (Profesores) dato;
        EmpleadoDao e = new EmpleadoDao();
        String[] columnas = super.columnas();
        String comando = "insert into Profesores";
        Empleados q = new Empleados(p.getNombres(), p.getApellidos(), p.getIdentificacion(), p.getCelular(), p.getGenero(), p.getDireccion(), TipoEmpleado.valueOf(p.getCargo()), p.getCorreo(), p.getPassword());
        String variables = "";
        System.out.println("*******************");

        for (int i = 0; i < columnas.length; i++) {
            if (i == columnas.length - 1) {
                variables += columnas[i];
            } else {
                variables += columnas[i] + " , ";
            }
        }
        e.guardarEmpleado(q);
        comando += "(" + variables + ") values(" + (super.contar() + 1) + "," + e.contar() + ",'" + p.getEspecialidad() + "')";
        try {
            PreparedStatement stmt = getConexion().prepareStatement(comando);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "guardado correctamente");
        } catch (SQLException ex) {
            System.out.println("Error en guardar " + ex);
        }

        System.out.println(comando);
        commit();
    }
    //    public void guardarProfesor(Object dato) throws Exception {
//        Profesores p = (Profesores) dato;
//        EmpleadoDao e = new EmpleadoDao();
//        String[] columnas = super.columnas();
//        String comando = "insert into Profesores";
//        Empleados q = new Empleados(p.getNombres(), p.getApellidos(), p.getIdentificacion(), p.getCelular(), p.getGenero(), p.getDireccion(), TipoEmpleado.valueOf(p.getCargo()), p.getCorreo(), p.getConstraseña());
//        String variables = "";
//        System.out.println("*******************");
//        for (int i = 0; i < columnas.length; i++) {
//            if (i == columnas.length - 1) {
//                variables += columnas[i];
//            } else {
//                variables += columnas[i] + " , ";
//            }
//        }
//        e.guardarEmpleado(q);
//        comando += "(" + variables + ") values(" + e.contar() + "," + super.contar() + 1 + ",'" + p.getEspecialidad() + "')";
//        try {
//            PreparedStatement stmt = getConexion().prepareStatement(comando);
//            stmt.executeUpdate();
//        } catch (SQLException ex) {
//            System.out.println("Error en guardar " + ex);
//        }
//
//        System.out.println(comando);
//        commit();
//    }
//

    public void updateProfesor(Object dato) throws Exception {
        Profesores p = (Profesores) dato;
        EmpleadoDao e = new EmpleadoDao();
//        UPDATE Empleados
//SET nombre = ads,apellido,1232,telefono = asd , genero = g,direccion= 72,correo = qwe,especialidad
//WHERE SalesOrderNum = identificacion;
        String[] columnas = super.columnas();
        String comando = "insert into Profesores";
        String update = "Update empleados set nombre='" + p.getNombres() + "',' apellido =" + p.getApellidos() + "',' telefono =" + p.getCelular() + "',' genero=" + p.getGenero() + "','direccion=" + p.getDireccion() + "','correo =" + p.getCorreo() + "','correo =" + p.getEspecialidad() + "')";

        System.out.println(update);
        String variables = "";
        for (int i = 0; i < columnas.length; i++) {
            if (i == columnas.length - 1) {
                variables += columnas[i];
            } else {
                variables += columnas[i] + " , ";
            }
        }
        comando += "(" + variables + ") values(" + (super.contar() + 1) + "," + e.contar() + ",'" + p.getEspecialidad() + "')";
        try {
            PreparedStatement stmt = getConexion().prepareStatement(comando);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "guardado correctamente");
        } catch (SQLException ex) {
            System.out.println("Error en guardar " + ex);
        }

        System.out.println(comando);
        commit();
    }

    public ListaEnlazada<Profesores> getProfesores(String atributo, Object o, Boolean m) {
        ListaEnlazada<Profesores> listaProfesores = new ListaEnlazada<>();

        String sql;
        if (m) {
            sql = "SELECT * FROM empleados,profesores where profesores.Id_empleado = empleados.Id_empleado and profesores." + atributo + " = " + " order by 1";
            System.out.println(sql);
        } else {
            sql = "SELECT * FROM empleados,profesores where profesores.Id_empleado = empleados.Id_empleado and " + atributo + " ='" + o + "' order by 1";
            System.out.println(sql);
        }

        try {
            PreparedStatement pps = getConexion().prepareStatement(sql);
            ResultSet resultado = pps.executeQuery();
            while (resultado.next()) {
                Profesores profesor = new Profesores(resultado.getInt("Id_profesore"), TipoEmpleado.P_PRO, resultado.getInt("id_empleado"), resultado.getString("correo"), resultado.getString("apellido"), resultado.getString("nombre"), resultado.getString("identificacion"), resultado.getString("telefono"), resultado.getString("genero").charAt(0), resultado.getString("direccion"), resultado.getString("especialidad"));
                listaProfesores.insertarCabecera(profesor);
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        return listaProfesores;
    }

    public ListaEnlazada<Profesores> getProfesoresNormal() {
        ListaEnlazada<Profesores> listaProfesores = new ListaEnlazada<>();
        String sql;
        sql = "SELECT * FROM empleados,profesores where profesores.Id_empleado = empleados.Id_empleado order by 1";
//        sql = "SELECT * FROM empleados,profesores where profesores.Id_empleado = empleados.Id_empleado and profesores order by 1";
        System.out.println(sql);

        try {
            PreparedStatement pps = getConexion().prepareStatement(sql);
            ResultSet resultado = pps.executeQuery();
            while (resultado.next()) {
                Profesores profesor = new Profesores(resultado.getInt("Id_profesore"), TipoEmpleado.P_PRO, resultado.getInt("id_empleado"), resultado.getString("correo"), resultado.getString("apellido"), resultado.getString("nombre"), resultado.getString("identificacion"), resultado.getString("telefono"), resultado.getString("genero").charAt(0), resultado.getString("direccion"), resultado.getString("especialidad"));
                listaProfesores.insertarCabecera(profesor);
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        return listaProfesores;
    }

    public void updateProfesor(String nombre, String apellido, String telefono, char genero, String direccion, String correo, String especialidad, String cedula) throws Exception {
        //  Profesores p =(Profesores) dato;
        // EmpleadoDao e = new EmpleadoDao();
//        UPDATE Empleados
//SET nombre = ads,apellido,1232,telefono = asd , genero = g,direccion= 72,correo = qwe,especialidad
//WHERE SalesOrderNum = identificacion;
        //String[] columnas = super.columnas();      
        //String comando= "insert into Profesores";
        //Update empleados set nombre ='juan',apellido ='marco', telefono ='0800899',genero = 'M',direccion='123-123',correo ='jordy.w.r',especialidad ='ciencias' where identificacion = ' 1150887105';
        System.out.println(nombre + apellido + telefono + genero + direccion + correo + cedula);
        String update = "Update empleados set nombre ='" + nombre + "',apellido ='" + apellido + "',telefono ='" + telefono + "', genero='" + genero + "',direccion='" + direccion + "',correo ='" + correo + "' where identificacion = '" + cedula + "'";

        System.out.println(update);
//    String variables = "";     
//for (int i = 0; i < columnas.length; i++) {
//            if (i == columnas.length-1) {
//                variables += columnas[i]; 
//            } else {    
//                variables += columnas[i] + " , ";
//            }
//        }
//    comando += "(" + variables + ") values("+(super.contar()+1)+","+e.contar()+",'"+p.getEspecialidad()+"')";
        try {
            PreparedStatement stmt = getConexion().prepareStatement(update);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "actualizado correctamente");
        } catch (SQLException ex) {
            System.out.println("Error en guardar " + ex);
        }
        commit();
    }
}
