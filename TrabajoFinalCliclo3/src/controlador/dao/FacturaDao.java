/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import modelo.SumKids.Estudiantes;
import modelo.SumKids.Facturas;
import modelo.enums.estadoMatricula;

/**
 *
 * @author User
 */
public class FacturaDao extends AdaptadorDao<Facturas> {

    private Facturas factura;

    public Facturas getFactura() {
        if (factura == null) {
            factura = new Facturas();

        }
        return factura;
    }

    public void setFactura(Facturas em) {
        this.factura = em;
    }

    public FacturaDao() {
        super(Facturas.class);
    }

    public Boolean guardarModificar() {
        try {
            if (this.getFactura().getId_factura() != null) {
                modificar(this.getFactura());

            } else {
                guardar(this.getFactura());
            }
            return true;
        } catch (Exception e) {
            System.out.println("Error" + e);
            return false;
        }
    }

    public void guardarFactura(Facturas dato) throws SQLException {
        Facturas fact = dato;
        String comando = "insert into facturas ";
        System.out.println("*******************");
        String fecha = dato.getFecha().get(Calendar.DATE) + "/" + dato.getFecha().get(Calendar.MONTH) + "/" + dato.getFecha().get(Calendar.YEAR);
        comando += " values (" + (super.contar() + 1) + "," + fact.getId_representante() + ",'" + fact.getPension() + ", to_date('" + fecha + "','DD/MM/YYYY')";

        try {
            PreparedStatement stmt = getConexion().prepareStatement(comando);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "guardado correctamente");
        } catch (Exception ex) {
            System.out.println("Error en guardar " + ex);
        }
        System.out.println(comando);
        commit();
    }
    public void guardarDetalle(Object [] dato) throws SQLException {
              String m = "detalles";
              String contar = "select count(id_";
        m = m.substring(0, m.length() - 1) + m.substring(m.length());
        //System.out.println(m);
        PreparedStatement stm = getConexion().prepareStatement(contar + m + " ) from detalles");
        ResultSet re = stm  .executeQuery();
        Integer a = 0;
        while (re.next()) {
            a = (Integer) re.getInt(1);
        }
        if (a == null) {
            a = 0;
        }
        String comando = "insert into detalles ";
        System.out.println("*******************");
        comando += " values (" +(a+1) + "," +super.contar()+ ",'" + (String.valueOf(dato[0]))  + "', " +Double.parseDouble(String.valueOf(dato[1]));

        try {
            
            PreparedStatement stmt = getConexion().prepareStatement(comando);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "guardado correctamente");
        } catch (Exception ex) {
            System.out.println("Error en guardar " + ex);
        }
        System.out.println(comando);
        commit();
  
    }
}
