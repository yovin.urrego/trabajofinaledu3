/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.dao;

import controlador.tda.lista.ListaEnlazada;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import modelo.SumKids.Estudiantes;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Locale;
import javax.swing.JOptionPane;
import modelo.SumKids.Profesores;
import modelo.SumKids.Representantes;
import modelo.enums.estadoMatricula;

/**
 *
 * @author Jordy
 */
public class EstudianteDao extends AdaptadorDao<Estudiantes> {

    private Estudiantes estudiante;

    public Estudiantes getEstudiante() {
        if (estudiante == null) {
            estudiante = new Estudiantes();

        }
        return estudiante;
    }

    public void setEstudiante(Estudiantes em) {
        this.estudiante = em;
    }

    public EstudianteDao() {
        super(Estudiantes.class);
    }

    public Boolean guardarModificar() {
        try {
            if (this.getEstudiante().getId_estudiante() != null) {
                modificar(this.getEstudiante());

            } else {
                guardar(this.getEstudiante());
            }
            return true;
        } catch (Exception e) {
            System.out.println("Error" + e);
            return false;
        }
    }
    
    public ListaEnlazada<Estudiantes> getEstudiantesNormal() {
        ListaEnlazada<Estudiantes> listaEstudiantes = new ListaEnlazada<>();
        String sql;
            sql = "Select * from estudiantes order by 1";
            System.out.println(sql);
        try {
            PreparedStatement pps = getConexion().prepareStatement(sql);
            ResultSet resultado = pps.executeQuery();
            while (resultado.next()) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(resultado.getDate("fecha_naci"));
                System.out.println("a qui no ");
                Estudiantes estudiantes = new Estudiantes(resultado.getInt("Id_estudiante"), resultado.getInt("id_representante"), cal, resultado.getString("apellido"), resultado.getString("nombre"), resultado.getString("identificacion"), resultado.getString("genero").charAt(0), resultado.getString("direccion"), estadoMatricula.valueOf(resultado.getString("Id_Estado_Matricula")), resultado.getDouble("pension"));
                listaEstudiantes.insertarCabecera(estudiantes);
                System.out.println("a qui");
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return listaEstudiantes;
    }

    public ListaEnlazada<Estudiantes> getEstudiantes(String atributo, Object o, Boolean m) {
        ListaEnlazada<Estudiantes> listaEstudiantes = new ListaEnlazada<>();

        String sql;
        if (m) {
            sql = "Select * from estudiantes where " + atributo + " = " + o + " order by 1";
            System.out.println(sql);
        } else {
            sql = "Select * from estudiantes where " + atributo + " = '" + o + "' order by 1";
            System.out.println(sql);
        }

        try {
            PreparedStatement pps = getConexion().prepareStatement(sql);
            ResultSet resultado = pps.executeQuery();
            while (resultado.next()) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(resultado.getDate("fecha_naci"));
                System.out.println("a qui no ");
                Estudiantes estudiantes = new Estudiantes(resultado.getInt("Id_estudiante"), resultado.getInt("id_representante"), cal, resultado.getString("apellido"), resultado.getString("nombre"), resultado.getString("identificacion"), resultado.getString("genero").charAt(0), resultado.getString("direccion"), estadoMatricula.valueOf(resultado.getString("Id_Estado_Matricula")), resultado.getDouble("pension"));
                listaEstudiantes.insertarCabecera(estudiantes);
                System.out.println("a qui");
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        return listaEstudiantes;
    }

//    public void guardarEstudiante(Object dato) throws SQLException {
//        Estudiantes e = (Estudiantes) dato;
//        String comando = "insert into Estudiantes";
//        String fecha = e.getFechaNacimiento().get(Calendar.DATE) + "/" + e.getFechaNacimiento().get(Calendar.MONTH) + "/" + e.getFechaNacimiento().get(Calendar.YEAR);
//        String variables = "";
//        comando += "(" + variables + ") values(" + (super.contar() + 1) + "," + e.getId_representante() + "," + ", to_date('" + fecha + "','DD/MM/YYYY'), '" + e.getNombres() + "','" + e.getApellidos() + "','" + e.getIdentificacion() + "','" + e.getGenero() + "','" + e.getDireccion() + "','" + estadoMatricula.ESPE + "','" + e.getPension() + "')";
//
//    }

//    public void guardarEstudiante(Object dato) throws Exception {
//        Estudiantes e = (Estudiantes) dato;
//        String[] columnas = super.columnas();
//        String comando = "insert into Estudiantes";
//        String variables = "";
//        System.out.println("*******************");
//
//        String fecha = e.getFechaNacimiento().get(Calendar.DATE) + "/" + e.getFechaNacimiento().get(Calendar.MONTH) + "/" + e.getFechaNacimiento().get(Calendar.YEAR);
//
//        for (int i = 0; i < columnas.length; i++) {
//            if (i == columnas.length - 1) {
//                variables += columnas[i];
//            } else {
//                variables += columnas[i] + " , ";
//            }
//        }
//        comando += "(" + variables + ") values(" + (super.contar() + 1) + "," + e.getId_representante() + "," + e.getId_profesor() + ", to_date('" + fecha + "','DD/MM/YYYY'), '" + e.getNombres() + "','" + e.getApellidos() + "','" + e.getIdentificacion() + "','" + e.getGenero() + "','" + e.getDireccion() + "')";
//
//        try {
//            PreparedStatement stmt = getConexion().prepareStatement(comando);
//            stmt.executeUpdate();
//            JOptionPane.showMessageDialog(null, "guardado correctamente");
//        } catch (SQLException ex) {
//            System.out.println("Error en guardar " + ex);
//        }
//
//        System.out.println(comando);
//        commit();
//    }
    public ListaEnlazada<Estudiantes> getEstudiantesConProfesor(String atributo, Object o, Boolean m) {
        ListaEnlazada<Estudiantes> listaEstudiantes = new ListaEnlazada<>();

        String sql;
        if (m) {
            sql = "SELECT * FROM empleados,profesores where profesores.Id_empleado = empleados.Id_empleado and profesores." + atributo + " = " + " order by 1";
            System.out.println(sql);
        } else {
            sql = "SELECT * FROM empleados,profesores where profesores.Id_empleado = empleados.Id_empleado and " + atributo + " ='" + o + "' order by 1";
            System.out.println(sql);
        }
        try {
            PreparedStatement pps = getConexion().prepareStatement(sql);
            ResultSet r = pps.executeQuery();
            while (r.next()) {
                Estudiantes estudiante = new Estudiantes(r.getInt("Id_estudiante"), r.getInt("id_representante"), Calendar.getInstance(), r.getString("apellidos"), r.getString("nombres"), r.getString("identificacion"), r.getString("genero").charAt(0), r.getString("direccion"), estadoMatricula.valueOf(r.getString("estado")), r.getDouble("pension"));
                listaEstudiantes.insertarCabecera(estudiante);
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        return listaEstudiantes;
    }

    public Boolean verificarEdad(int anio) {
        Calendar c = Calendar.getInstance();
        Boolean verificado;
        int f = c.get(Calendar.YEAR) - anio;
        if (f <= 10) {
            verificado = true;

        } else {
            verificado = false;
        }
        return verificado;
    }

    public Double calcularPension(int i, String m) {
        Double pension = 0.0;
        switch (i) {
            case 2:
                if (m.equalsIgnoreCase("Ingles") || m.equalsIgnoreCase("Terapia de lenguaje")) {
                    pension = 40.0;
                } else {
                    pension = 30.0;
                }
                break;
            case 3:
                if (m.equalsIgnoreCase("Ingles") || m.equalsIgnoreCase("Terapia de lenguaje")) {
                    pension = 60.0;
                } else {
                    pension = 50.0;
                }
                break;
            case 5:
                if (m.equalsIgnoreCase("Ingles") || m.equalsIgnoreCase("Terapia de lenguaje")) {
                    pension = 80.0;
                } else {
                    pension = 70.0;
                }
                break;
        }
        return pension;
    }

    public void guardarEstudiante(Object dato) throws SQLException {
        Estudiantes e = (Estudiantes) dato;
        String comando = "insert into Estudiantes";
        String comando2 = "insert into est_mat";
        String fecha = e.getFechaNacimiento().get(Calendar.DATE) + "/" + e.getFechaNacimiento().get(Calendar.MONTH) + "/" + e.getFechaNacimiento().get(Calendar.YEAR);
        String variables = "";
        Calendar ca = Calendar.getInstance();
        String[] columnas = super.columnas();

        for (int i = 0; i < columnas.length; i++) {
            if (i == columnas.length - 1) {
                variables += columnas[i];
            } else {
                variables += columnas[i] + " , ";
            }
        }
        //System.out.println(e.getId_representante());
        comando += "(" + variables + ") values(" + (super.contar() + 1) + "," + e.getId_representante() + "," + " to_date('" + fecha + "','DD/MM/YYYY'), '" + e.getNombres() + "','" + e.getApellidos() + "','" + e.getIdentificacion() + "','" + e.getGenero() + "','" + e.getDireccion() + "','" +e.getEstado()+ "','" + e.getPension() + "')";

        System.out.println(comando);
        try {
            PreparedStatement stmt = getConexion().prepareStatement(comando);
            stmt.executeUpdate();
            //JOptionPane.showMessageDialog(null, "guardado correctamente");
        } catch (SQLException ex) {
            System.out.println("Error en guardar " + ex);
        }

      //  comando += "(" + variables + ") values(" + (super.contar() + 1) + "," + e.getId_representante() + "," + ", to_date('" + fecha + "','DD/MM/YYYY'), '" + e.getNombres() + "','" + e.getApellidos() + "','" + e.getIdentificacion() + "','" + e.getGenero() + "','" + e.getDireccion() + "','" + estadoMatricula.ESPE + "','" + e.getPension() + "')";

    }
public void updateEstudiante(String direccion,String cedula) throws Exception {
        
        String update = "Update estudiantes set direccion ='" + direccion +"' where identificacion = '" + cedula + "'";

        System.out.println(update);
        try {
            PreparedStatement stmt = getConexion().prepareStatement(update);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "actualizado correctamente");
        } catch (SQLException ex) {
            System.out.println("Error en guardar " + ex);
        }
        commit();
    }
    public static void main(String[] args) throws Exception {

        Date d = new Date();
        Calendar r = Calendar.getInstance();
        r.set(2016, 10, 1);
//         DD/MON/YY
        //EstudianteDao e = new EstudianteDao();
        //   Estudiantes w= new Estudiantes();
        System.out.println(new SimpleDateFormat("dd/MM/YYYY").format(r.getTime()));
        //   e.guardarEstudiante(w);

        //    System.out.println(r.get(Calendar.MONTH- r.));
        System.out.println(r.get(Calendar.MONTH));
//        Date d = new Date();
//        Calendar r = Calendar.getInstance();
//        r.set(2016, 10, 1);
////         DD/MON/YY
//        //EstudianteDao e = new EstudianteDao();
//        //   Estudiantes w= new Estudiantes();
//        System.out.println(new SimpleDateFormat("dd/MM/YYYY").format(r.getTime()));
//        //   e.guardarEstudiante(w);
//        System.out.println(r.get(Calendar.MONTH));

        EstudianteDao m = new EstudianteDao();
        Calendar fecha = Calendar.getInstance();
        //m.verificarEdad(2020);
        System.out.println(m.verificarEdad(2000));
        //fecha.set(2016, 10, 1);
        //Calendar fecha, Integer id_profesor, ,String apellidos, String nombres, String identificacion, char sexo, String direccion
        //    Estudiantes e = new Estudiantes(fecha, 8, 1, "Angamarca", "Brayan", "1212121212", 'M', "salinassssssss");
        //     m.guardarEstudiante(e);
    }
}
