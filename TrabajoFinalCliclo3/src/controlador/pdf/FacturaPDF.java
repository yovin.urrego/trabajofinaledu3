package controlador.pdf;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import controlador.tda.lista.ListaEnlazada;
import controlador.tda.lista.exception.PosicionException;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.SumKids.Empleados;
import modelo.SumKids.Facturas;

/**
 *
 * @author Yovin
 */
public class FacturaPDF {

//    public void tablaPDF(String nomEmpresa, String art, String cod, ,PlantillaPDF pdf) {
//        ControladorPlantillaPDF doc = new ControladorPlantillaPDF(pdf.getNombreUsuario(),pdf.getNombreArchivo());
//        Image im = null;
//        try {
//            im = new Image(ImageDataFactory.create("C:\\Users\\LENOVO\\OneDrive\\Escritorio\\CCB\\src\\Vista\\Imagen\\logo.png"));
//            im.scaleAbsolute(150, 100).setFixedPosition(670,494);
//        } catch (MalformedURLException e) {
//            System.out.println("no se encuentra la imagen ");
//        }
//        Table table = new Table(11);
//        table.addCell(new Cell(1, 11).setTextAlignment(TextAlignment.CENTER).add(new Paragraph(nomEmpresa)));
//        table.addCell(new Cell(1, 11).setTextAlignment(TextAlignment.CENTER).add(new Paragraph("TARJETA KARDEX")));
//        table.addCell(new Cell(1, 4).setTextAlignment(TextAlignment.LEFT).add(new Paragraph("Articulo: " + art)));
//        table.addCell(new Cell(1, 3).setTextAlignment(TextAlignment.LEFT).add(new Paragraph("Codigo: " + cod)));
//        table.addCell(new Cell(1, 4).setTextAlignment(TextAlignment.LEFT).add(new Paragraph("Metodo : Promedio ")));
//        table.addCell(new Cell(2, 1).setTextAlignment(TextAlignment.CENTER).add(new Paragraph("Fecha"))).addCell(new Cell(2, 1).setTextAlignment(TextAlignment.CENTER).add(new Paragraph("Detalle"))).addCell(new Cell(1, 3).setTextAlignment(TextAlignment.CENTER).add(new Paragraph("ENTRADAS"))).addCell(new Cell(1, 3).setTextAlignment(TextAlignment.CENTER).add(new Paragraph("SALIDAS"))).addCell(new Cell(1, 3).setTextAlignment(TextAlignment.CENTER).add(new Paragraph("SALDOS")));
//        table.addCell("Cant").addCell("Vr/U").addCell("Total").addCell("Cant").addCell("Vr/U").addCell("Total").addCell("Cant").addCell("Vr/U").addCell("Total");
//        for (ControladorKardex kar : kars) {
//            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//            String dateToStr = dateFormat.format(kar.kar1.getFecha());
//            table.addCell(new Cell().setTextAlignment(TextAlignment.LEFT).add(new Paragraph(dateToStr))).addCell(new Cell().setTextAlignment(TextAlignment.LEFT).add(new Paragraph(kar.kar1.getDetalle())));
//            celdas(table, kar.kar1.getCantidadEntrada(), kar.kar1.getValorUnitarioEntrada(), kar.kar1.getTotalEntrada());
//            celdas(table, kar.kar1.getCantidadSalida(), kar.kar1.getValorUnitarioSalida(), kar.kar1.getTotalSalida());
//            celdas(table, kar.kar1.getCantidadTotal(), kar.kar1.getValorUnitarioTotal(), kar.kar1.getTotalTotal());
//        }
//        doc.CrearPlantilla(PageSize.A4.rotate(),im,table);
//    }
//    public void celdas(Table table, Integer num, Double num2, Double num3) {
//        table.addCell(new Cell().setTextAlignment(TextAlignment.RIGHT).add(new Paragraph(Integer.toString(num))))
//                .addCell(new Cell().setTextAlignment(TextAlignment.RIGHT).add(new Paragraph(Double.toString(num2))))
//                .addCell(new Cell().setTextAlignment(TextAlignment.RIGHT).add(new Paragraph(Double.toString(num3))));
//    }
    public void tablaPDF(Facturas fac, Empleados remitente, ListaEnlazada<Object[]> datelles) throws IOException, PosicionException {
        File file = new File("datos" + File.separatorChar + "facturas" + File.separatorChar + "Factura" + fac.getId_factura() + ".pdf");
        //ControladorPlantillaPDF doc = new ControladorPlantillaPDF(pdf.getNombreUsuario(),pdf.getNombreArchivo());
        PdfWriter pdfW = new PdfWriter(file);
        PdfDocument docPDF = new PdfDocument(pdfW);
        Document document = new Document(docPDF, PageSize.A4);
        document.add(new Paragraph("SumKids Club "));
        document.add(new Paragraph("" + fac.getId_factura()));
        document.add(new Paragraph(" "));
        document.add(new Paragraph(" "));
        document.add(new Paragraph("Cliente: " + fac.getApellido() + "  " + fac.getNombre() + "                  Cel: " + fac.getIdentificacion()));
        document.add(new Paragraph("Remitente: " + remitente.getApellidos() + "  " + remitente.getNombres() + "                  Cel: " + remitente.getIdentificacion()));
        Table table = new Table(20);
        
        table.addCell(new Cell(1, 15).setTextAlignment(TextAlignment.CENTER).add(new Paragraph(" Detalle ............................................................................................................")));
        table.addCell(new Cell(1, 5).setTextAlignment(TextAlignment.LEFT).add(new Paragraph("Costo")));
         Double costo=0.00;
        for (int i = 0; i < datelles.getSize(); i++) {
           costo+= Double.valueOf(datelles.obtenerDato(i)[1].toString());
            table.addCell(new Cell(1, 15).setTextAlignment(TextAlignment.CENTER).add(new Paragraph("" + datelles.obtenerDato(i)[0])));
            table.addCell(new Cell(1, 5).setTextAlignment(TextAlignment.CENTER).add(new Paragraph("" + datelles.obtenerDato(i)[1])));     
        }
          document.add(table);
  document.add(new Paragraph(".............................................................................................................................  "+costo));
      

        document.close();
    }
    
//    public static void main(String[] args) throws PosicionException {
//        try {
//            FacturaPDF p = new FacturaPDF();
//            Facturas fac = new Facturas();
//            Empleados emp = new Empleados();
//            ListaEnlazada list[] = new ListaEnlazada[2];
//           list[0] = new ListaEnlazada();
//                    list[0]    .insertarCabecera(10);
//            list[0].insertarCabecera("Est 01");
//                 list[1] = new ListaEnlazada();
//               list[1].insertarCabecera(20);
//            list[1].insertarCabecera("Est 02");
//            p.tablaPDF(fac, emp, list);
//        } catch (IOException ex) {
//            Logger.getLogger(FacturaPDF.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//    }
    
}
