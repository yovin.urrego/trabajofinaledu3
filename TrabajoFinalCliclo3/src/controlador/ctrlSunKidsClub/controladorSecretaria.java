/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ctrlSunKidsClub;

import controlador.conexion.SQLclass;
import controlador.dao.EmpleadoDao;
import controlador.dao.EstudianteDao;
import controlador.dao.FacturaDao;
import controlador.dao.ProfesorDao;
import controlador.dao.RepresentanteDao;
import controlador.tda.lista.ListaEnlazada;
import java.sql.SQLException;
import modelo.SumKids.Empleados;
import modelo.SumKids.Estudiantes;
import modelo.SumKids.Facturas;
import modelo.SumKids.Profesores;
import modelo.SumKids.Representantes;

/**
 *
 * @author Jose Yangari
 */
public class controladorSecretaria {

    private ProfesorDao pro = new ProfesorDao();

    private RepresentanteDao rep = new RepresentanteDao();
  private EmpleadoDao empl = new EmpleadoDao();
   private  EstudianteDao estu = new EstudianteDao();
   private  FacturaDao fac = new FacturaDao();

    //Profesores
    public void guardarProfesor(Profesores profesor) throws Exception {
        pro.guardarProfesor(profesor);
    }

    public ListaEnlazada<Profesores> busacarProfesor(Object o, String atributo, boolean b) {
        return pro.getProfesores(atributo, o, b);
    }

    public ListaEnlazada<Profesores> listarProfesores() {
        return pro.getProfesoresNormal();

    }

    public void updateProfesor(String nombre, String apellido, String telefono, char genero, String direccion, String correo, String especialidad, String cedula) throws Exception {
        pro.updateProfesor(nombre, apellido, telefono, genero, direccion, correo, especialidad, cedula);
    }

    //Empleados
    public void guardarEmpleado(Empleados empleado) throws Exception {
        empl.guardarEmpleado(empleado);
    }

    public ListaEnlazada<Empleados> busacarEmple(Object o, String atributo, boolean b) throws SQLException {
        return empl.objetenerEmple(atributo, o, b);
    }

    //Representantes
    public void guardarRepresentante(Representantes representante) throws Exception {
        rep.guardarRepresentante(representante);
    }

    public ListaEnlazada<Representantes> busacarRepresentantes(Object o, String atributo, boolean b) {
        return rep.getRepresentantes(atributo, o, b);
    }

    public Object[] busacarRepresentante(String atributo) throws Exception {
        return rep.obtenerD(atributo);
    }
    
    public void updateRepresentante(String nombre, String apellido, String telefono, char genero, String direccion, String cedula) throws Exception {
        rep.updateRepresentante(nombre, apellido, telefono, genero, direccion, cedula);
    }

    //Estudiantes
    public void guardarEstudiante(Estudiantes estudiante) throws Exception {
        estu.guardarEstudiante(estudiante);
    }
   public void updateEstudiantes( String direccion, String cedula) throws Exception{
       estu.updateEstudiante(direccion,cedula);
   }
    public ListaEnlazada<Estudiantes> busacarEstudiantes(Object o, String atributo, boolean b) {
        return estu.getEstudiantes(atributo, o, b);
    }
    
    public ListaEnlazada<Estudiantes> listarEstudiantes() {
        return estu.getEstudiantesNormal();

    }
   

    //Factura
    public void guardarFacturas(Facturas dato, Object[] detalle) throws Exception {
        fac.guardarFactura(dato);
        fac.guardarDetalle(detalle);
    }

    public Double calcularPension(int horas, String materia) {
        return estu.calcularPension(horas, materia);
    }

    public Integer nFacturas() throws SQLException {
        return fac.contar() + 1;
    }
    public Boolean verficarEdad(int m){
        return estu.verificarEdad(m);
    }

}
