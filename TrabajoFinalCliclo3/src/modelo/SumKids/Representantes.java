/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.SumKids;

import modelo.SumKids.Persona;

/**
 *
 * @author User
 */
public class Representantes extends Persona {

    private Integer id_representante;

    public Representantes() {
    }
    //Costructur para retornar de base de datos
    public Representantes(Integer id_representante, String nombres, String apellidos, String identificacion, String celular, char gene, String direccion) {
        super(nombres, apellidos, identificacion, celular, gene, direccion);
        this.id_representante = id_representante;
    }
    
    //Constructor normal
    public Representantes(String nombres, String apellidos, String identificacion, String celular, char sexo, String direccion) {
        super(nombres, apellidos, identificacion, celular, sexo, direccion);
        
    }

    public Representantes(String identificacion,Integer id_representante) {
        super(identificacion);
        this.id_representante = id_representante;
    }

    public Integer getId_representante() {
        return id_representante;
    }

    public void setId_representante(Integer id_representante) {
        this.id_representante = id_representante;
    }

}
