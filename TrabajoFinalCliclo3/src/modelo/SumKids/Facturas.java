/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.SumKids;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author User
 */
public class Facturas {

    private Integer id_factura = null;
    private Integer id_representante = null;
    private Double pension = null;
    private Calendar fecha = null;
    private String nombre = null;
    private String apellido = null;
    private String direccion = null;
    private String identificacion = null;
    private String celular = null;

    public Facturas() {
    }

    public Facturas(Integer nro, Integer id_representante, Double pension, Calendar fecha) {
        this.id_factura = nro;
        this.id_representante = id_representante;
        this.pension = pension;
        this.fecha = fecha;
    }

    public Integer getId_factura() {
        return id_factura;
    }

    public void setId_factura(Integer nro) {
        this.id_factura = nro;
    }

    public Integer getId_representante() {
        return id_representante;
    }

    public void setId_representante(Integer id_representante) {
        this.id_representante = id_representante;
    }

    public Double getPension() {
        return pension;
    }

    public void setPension(Double pension) {
        this.pension = pension;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

}
